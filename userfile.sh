#!/bin/sh

# Funtion to display the welcome message
display_welcome_message () {
    USERNAME=$1
    echo "Hello, $USERNAME."
    echo "Welcome to the User File Creation Script"
}

# Function to show menu
show_menu () {
    MENU_TEXT="--------------MENU------------\n
            1. Create File\n
            2. Edit File\n
            3. Read File\n
            4. Exit\n
    "
    echo $MENU_TEXT
    echo "Enter your option: "
}

#  Function to check if the user exists or not
search_user_file () {
    USERFILE=$1
    FOUND=0
    mkdir -p name_file
    cd name_file
    for file in *.txt
    do
        if [ "$file" = "${USERFILE}_file.txt" ];
        then
            FOUND=1
            break
        fi
    done
    cd ..
    return "$FOUND"
}

# Function to create user file
create_userfile () {
    USERNAME=$1
    mkdir -p name_file
    cd ./name_file
    echo "Creating a file for user $USERNAME"
    touch "${USERNAME}_file.txt"
    echo "Hello, $USERNAME. This is the file you created!!" > "${USERNAME}_file.txt"
    echo "File created successfully"
    echo "The content of the files is: "
    cat "${USERNAME}_file.txt"
    cd ..
}

# The gloabl variable for finding file
FOUND=0

# MAIN
echo "Enter your name: "
read USERNAME
display_welcome_message $USERNAME

while :
do
    show_menu
    read option
    # Create files if the user does not exist
    if [ "$option" = "1" ];
    then
        search_user_file $USERNAME
        F=$?
        if [ $F -eq 1 ];
        then
            echo "User ${USERNAME}'s file already exists"
        else
            create_userfile $USERNAME
        fi
        echo "Do you want to continue?[y for yes]: "
        read option1
        if [ "$option1" != "y" ];
        then
            break
        fi
    # Editing File [Replaces the whole document]
    elif [ "$option" = "2" ]; then
        search_user_file $USERNAME
        F=$?
        if [ $F -eq 0 ];
        then
            echo "User ${USERNAME}'s file does not exist!"
        else
            echo "Enter what you want to wrtie in the file: "
            read text
            cd ./name_file
            echo $text > "${USERNAME}_file.txt"
            cd ..
        fi
    # Read the file if user exist
    elif [ "$option" = "3" ]; then
        search_user_file $USERNAME
        F=$?
        if [ $F -eq 0 ];
        then
            echo "User ${USERNAME}'s file does not exist!"
        else
            cd ./name_file 
            cat "${USERNAME}_file.txt"
            cd ..
        fi
    # Exit
    elif [ "$option" = "4" ]; then
        echo "Thank you for using the User file creation app"
        break
    else
        echo "Invalid Option!!"
    fi
done
